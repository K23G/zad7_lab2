package pl.edu.wat;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.*;
import com.github.javaparser.ast.body.*;

import javax.tools.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException {
        final String fileName = "src\\Class.java";
        final String alteredFileName = "src\\ClassAltered.java";
        CompilationUnit cu;
        try (FileInputStream in = new FileInputStream(fileName)) {
            cu = JavaParser.parse(in);
        }

        /*
        Sortowanie metod, dodatkowo aby poprawić prezentowanie się klasy, posortowane zostały pola. Dzięki temu nowa klasa
        tworzona jest w taki sposób aby zachować kolejność, że to pola są wcześniej niż metody.

        Sortowanie według podanego schematu: private -> no modifier -> protected -> public. Następnie po liczbie parametrów.
        Metody, które są package-private(bez modyfikatora) pozwalają na mniejszy dostęp dlatego zostały ustawione po metodach private.
         */
        changeMethods(cu);

        cu.getClassByName("Class").get().setName("ClassAltered");

        try (FileWriter output = new FileWriter(new File(alteredFileName), false)) {
            output.write(cu.toString());
        }

        File[] files = {new File(alteredFileName)};
        String[] options = {"-d", "out//production//Synthesis"};

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
        try (StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null)) {
            Iterable<? extends JavaFileObject> compilationUnits =
                    fileManager.getJavaFileObjectsFromFiles(Arrays.asList(files));
            compiler.getTask(
                    null,
                    fileManager,
                    diagnostics,
                    Arrays.asList(options),
                    null,
                    compilationUnits).call();

            diagnostics.getDiagnostics().forEach(d -> System.out.println(d.getMessage(null)));
        }
    }


    private static void changeMethods(CompilationUnit pi) {
        List<MethodDeclaration> methods = new LinkedList<>();
        List<ClassOrInterfaceDeclaration> classes = new LinkedList<>();
        List<FieldDeclaration> fields = new LinkedList<>();
        NodeList<TypeDeclaration<?>> types = pi.getTypes();

        for (TypeDeclaration<?> type : types) {
            NodeList<BodyDeclaration<?>> members = type.getMembers();
            for (BodyDeclaration<?> member : members) {
                if (member instanceof MethodDeclaration) {
                    methods.add((MethodDeclaration) member);
                }
                if (member instanceof ClassOrInterfaceDeclaration) {
                    classes.add((ClassOrInterfaceDeclaration) member);
                }
                if (member instanceof FieldDeclaration) {
                    fields.add((FieldDeclaration) member);
                }
            }
        }
        List<MethodDeclaration> sortedMethods = methods.stream()
                .sorted(Main::compareMethods)
                .collect(Collectors.toList());
        List<ClassOrInterfaceDeclaration> sortedClasses = classes.stream()
                .sorted()
                .collect(Collectors.toList());
        List<FieldDeclaration> sortedFields = fields.stream()
                .sorted((f1, f2) -> {
                    StringBuilder f1String = new StringBuilder();
                    StringBuilder f2String = new StringBuilder();
                    createSpecifierString(f1String, f1.getModifiers());
                    createSpecifierString(f2String,f2.getModifiers());
                    return f1String.toString().compareTo(f2String.toString());
                })
                .collect(Collectors.toList());

        methods.forEach(Node::remove);
        classes.forEach(Node::remove);
        fields.forEach(Node::remove);


        for (FieldDeclaration fieldDeclaration : sortedFields) {
            pi.getTypes().get(0).addMember(fieldDeclaration);
        }
        for (MethodDeclaration m : sortedMethods) {
            pi.getTypes().get(0).addMember(m);
        }
        for (ClassOrInterfaceDeclaration classOrInterfaceDeclaration : sortedClasses) {
            pi.getTypes().get(0).addMember(classOrInterfaceDeclaration);
        }
    }

    private static void createSpecifierString(StringBuilder stringBuilder, EnumSet<Modifier> modifiers) {
        AccessSpecifier accessSpecifier = Modifier.getAccessSpecifier(modifiers);
        if (accessSpecifier == AccessSpecifier.DEFAULT) {
            stringBuilder.append("privatep");
        } else {
            stringBuilder.append(accessSpecifier.asString());
        }
    }

    private static StringBuilder methodString(MethodDeclaration method) {
        StringBuilder methodString = new StringBuilder();
        createSpecifierString(methodString, method.getModifiers());

        return methodString;
    }

    private static int compareMethods(MethodDeclaration m1, MethodDeclaration m2) {
        StringBuilder m1String = methodString(m1);
        StringBuilder m2String = methodString(m2);

        int m1size = m1.getParameters().size();
        int m2size = m2.getParameters().size();

        int i = m1String.toString().compareTo(m2String.toString());

        if (i == 0) {
            if (m1size < m2size) return -1;
            else if (m1size == m2size) return 0;
            else return 1;
        } else {
            return i;
        }
    }
}
